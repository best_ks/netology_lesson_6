// Сегодня мы печем пироги!
	  
	  // Вам нужно испечь по 10 пирогов каждого типа
	  // Можно использовать все знания, которые вы получили на наших лекциях

	  // Рецепты:
	  // Яблочный пирог (3 кг яблок, 2 кг. муки, 1 литр молока, 3 яйца)
	  // Клубничный пирог (5 кг клубники, 1 кг. муки, 2 литр молока, 4 яйца)
	  // Абрикосовый пирог (2 кг абрикос, 3 кг. муки, 2 литр молока, 2 яйца)

	  // Запасы на складе
	  var apples = 20; // Яблоки, кг
	  var strawberry = 20; // Клубника - 75 р/кг
	  var apricot = 20; // Абрикос - 35 р/кг
	  var flour = 20; // Мука - 10 р/кг
	  var milk = 20; // Молоко - 15 р/литр
	  var eggs = 50; // Яйца - 3 р/шт
	  var applePie = 0; // количество яблочных пирогов, которое испекли.
	  var strawberryPie = 0; // количество клубничных пирогов, которое испекли.
	  var apricotPie = 0; // количество абрикосовых пирогов, которое испекли.

    //массивы, описывающие рецепты пирогов.
    //7 и 8 элементы фиксируют оставшееся количество основного ингридиета на складе
    //и количество изготовленных пирогов соответственно.
    var applePieRecept = ['яблоки', 3, 2, 1, 3, 'Яблочный', apples, applePie];
    var strawberryPieRecept = ['клубника', 5, 1, 2, 4, 'Клубничный', strawberry, strawberryPie];
    var apricotPieRecept = ['абрикосы', 2, 3, 2, 2, 'Абрикосовый', apricot, apricotPie];

    //Функция, вызываемая в случае, если закончился ингридент
    function needMore(nameElement, numberElement){  // nameElement - название ингридиента, numberElement - количество ингридиента
        
        if (typeof(nameElement)=='string')
        {
            if (typeof(numberElement)=='number'){
                var str1 = 'Кончились', str2 = 'кг';
                if(nameElement=='мука'|| nameElement=='клубника'){
                    str1 = 'Кончилась';
                }
                else if (nameElement=='молоко'){
                    str1 = 'Кончилось';
                    str2 = 'литров';
                }
                else if (nameElement=='яйца'){
                    str2 = 'штук';
                }
                console.log('%s %s! Купили еще 10 %s', str1, nameElement, str2);
                return numberElement+10;
            }
            else console.log('Вторым параметром в функцию needMore передано не число!');
        }
        else console.log('Первым параметром в функцию needMore передана не строка!');
    }

    //Функция по выпеканию пирогов
    function cookPieFunction(recept){
        if (recept[6] - recept[1] < 0) {
            recept[6] = needMore(recept[0],recept[6]);
        } else if (flour - recept[2] < 0) {
            flour = needMore('мука',flour);
        } else if (milk - recept[3] < 0) {
            milk = needMore('молоко',milk);
        } else if (eggs - recept[4] < 0) {
	  	    eggs = needMore('яйца',eggs);
        } else {
            recept[6] = recept[6] - recept[1];  //подсчет остатка основного игридиента
            flour = flour - recept[2];          //остаток муки на складе
            milk = milk - recept[3];            //остаток молока на складе
            eggs = eggs - recept[4];            //остаток яиц на складе
            recept[7]++;
            console.log('%s пирог %d готов!', recept[5], recept[7]);
	   }
    }

    //цикл выпекания каждого пирога в количестве 10 штук
    do {
        if(applePie!=10){
            cookPieFunction(applePieRecept);
            apples=applePieRecept[6];               //передача данных на склад об оставшемся количестве яблок
            applePie=applePieRecept[7];             //передача данных на склад об изготовленном количестве яблочных пирогов
        }
        if(strawberryPie!=10){
            cookPieFunction(strawberryPieRecept);
            strawberry=strawberryPieRecept[6];      //передача данных на склад об оставшемся количестве клубники
            strawberryPie=strawberryPieRecept[7];   //передача данных на склад об изготовленном количестве клубничных пирогов
        }
        if(apricotPie!=10){
            cookPieFunction(apricotPieRecept);
            apricot=apricotPieRecept[6];            //передача данных на склад об оставшемся количестве абрикосов
            apricotPie=apricotPieRecept[7];         //передача данных на склад об изготовленном количестве абрикосовых пирогов
        }
    } while((applePie!=10) || (strawberryPie!=10) || (apricotPie!=10));