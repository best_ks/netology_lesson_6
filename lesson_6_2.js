//с помощью цикла
function sumTo(n){
    var sum=0;
    do{
        sum+=n--;
    }while(n!=0);
    return sum;
}
console.log('Решение с помощью цикла: '+sumTo(5));

//с помощью рекурсии
function sumToRec(n){
    if(n!=1)
        return n+sumToRec(--n);
    else
        return n;
}
console.log('Решение с помощью рекурсии: '+sumToRec(1));